package api;

import pojo.PetDto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PetStoreApi {

    @GET("/v2/pet/{petId}")
    Call<PetDto> getPet(@Path("petId") Long petId);

    @POST("/v2/pet")
    Call<PetDto> createPet(@Body PetDto dto);
}
