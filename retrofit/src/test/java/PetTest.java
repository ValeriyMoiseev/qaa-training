import static java.net.HttpURLConnection.HTTP_OK;

import io.qameta.allure.Allure;
import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pojo.PetDto;
import pojo.PetDto.Item;
import retrofit2.Response;

public class PetTest extends BaseTest {

    @Test
    public void createPet() {
        PetDto requestDto = PetDto.builder()
                .id(777L)
                .name("Barsik")
                .status("ACTIVE")
                .photoUrls(new ArrayList<>())
                .tags(new ArrayList<>())
                .category(new Item())
                .build();

        Allure.step("Создать питомца", () -> {
            Response<PetDto> response = api.createPet(requestDto).execute();
            Assertions.assertEquals(HTTP_OK, response.code());
            assert response.body() != null;
            Assertions.assertEquals(requestDto.getId(), response.body().getId());
        });

        Allure.step("Получить созданного питомца", () -> {
            Response<PetDto> response = api.getPet(requestDto.getId()).execute();
            Assertions.assertEquals(HTTP_OK, response.code());
            assert response.body() != null;
            Assertions.assertEquals(requestDto.getId(), response.body().getId());
        });
    }
}
