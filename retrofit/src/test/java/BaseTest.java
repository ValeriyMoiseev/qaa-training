import static retrofit.RetrofitBuilder.getRetrofit;

import api.PetStoreApi;

public class BaseTest {

    private final static String BASE_URL = "https://petstore.swagger.io";
    protected PetStoreApi api = getRetrofit(BASE_URL).create(PetStoreApi.class);
}
